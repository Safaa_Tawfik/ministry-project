import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsModule } from './modules/about-us/about-us.module';
import { AdminDashboardModule } from './modules/admin-dashboard/admin-dashboard.module';
import { CircularsModule } from './modules/circular/circulars.module';
import { BaseComponent } from './modules/layout/base/base.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'circulars',
    pathMatch: 'full',
  },
  {
    path: '',
    component: BaseComponent,
    children: [
      {
        path: 'admin',
        loadChildren: () =>
          import('./modules/admin-dashboard/admin-dashboard.module').then(
            (m) => m.AdminDashboardModule
          ),
        // data: { breadcrumb: 'Breadcrumb.discussionsRoute.discussions' },
      },
      {
        path: 'about',
        loadChildren: () =>
          import('./modules/about-us/about-us.module').then(
            (m) => m.AboutUsModule
          ),
        // data: { breadcrumb: 'Breadcrumb.discussionsRoute.discussions' },
      },
      {
        path: 'circulars',
        loadChildren: () =>
          import('./modules/circular/circulars.module').then(
            (m) => m.CircularsModule
          ),
        // data: { breadcrumb: 'Breadcrumb.discussionsRoute.discussions' },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
