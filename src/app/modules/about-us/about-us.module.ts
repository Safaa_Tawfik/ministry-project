import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSpinner } from '@angular/material/progress-spinner';
import { AboutUsRoutingModule } from './about-us-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,AboutUsRoutingModule
  ]
})
export class AboutUsModule { }
