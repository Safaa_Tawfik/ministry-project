import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHomeComponent } from './admin-home/admin-home.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: AdminHomeComponent,
      } ]
      //, children: [
    //   {
    //     path: '',
    //     component: AboutUsComponent,
    //   }
    //   ,{
    //     path: 'new',
    //     component: DiscussionFormComponent,
    //     data: { breadcrumb: 'Breadcrumb.discussionsRoute.AddDiscussion' },
    //   },
    //   {
    //     path: 'edit/:id',
    //     component: DiscussionFormComponent,
    //     data: { breadcrumb: 'Breadcrumb.discussionsRoute.editdiscussion' },
    //   },
    //   {
    //     path: 'details/:id',
    //     data: { breadcrumb: 'Breadcrumb.discussionsRoute.DiscussionDetails' },
    //     children: [
    //       {
    //         path: '',
    //         component: DiscussionDetailsComponent,
    //       },

    //       {
    //         path: 'edit',
    //         component: DiscussionFormComponent,
    //         data: { breadcrumb: 'Breadcrumb.discussionsRoute.editdiscussion' },
    //       },
     //   ],
      //},
  //  ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminDashboardRoutingModule {}
