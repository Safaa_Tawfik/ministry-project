import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { CircularsModule } from '../circular/circulars.module';
import { AboutUsRoutingModule } from '../about-us/about-us-routing.module';
import { AdminDashboardRoutingModule } from './admin-dashboard-routing.module';



@NgModule({
  declarations: [AdminHomeComponent],
  imports: [
    CommonModule,AdminDashboardRoutingModule,CircularsModule,AboutUsRoutingModule
  ]
})
export class AdminDashboardModule { }
