import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCircularComponent } from './components/add-circular/add-circular.component';
import { CircularsComponent } from './components/circulars/circulars.component';
import { PdfViewerComponent } from './components/pdf-viewer/pdf-viewer.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CircularsComponent,
      }
      ,{
        path: 'new',
        component: AddCircularComponent,
       // data: { breadcrumb: 'Breadcrumb.discussionsRoute.AddDiscussion' },
      },
      {
        path: 'viewer/:filename',
        component: PdfViewerComponent
      }
    //   {
    //     path: 'edit/:id',
    //     component: DiscussionFormComponent,
    //     data: { breadcrumb: 'Breadcrumb.discussionsRoute.editdiscussion' },
    //   },
    //   {
    //     path: 'details/:id',
    //     data: { breadcrumb: 'Breadcrumb.discussionsRoute.DiscussionDetails' },
    //     children: [
    //       {
    //         path: '',
    //         component: DiscussionDetailsComponent,
    //       },

    //       {
    //         path: 'edit',
    //         component: DiscussionFormComponent,
    //         data: { breadcrumb: 'Breadcrumb.discussionsRoute.editdiscussion' },
    //       },
     //   ],
      //},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CircularsRoutingModule {}
