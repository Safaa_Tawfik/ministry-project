import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CircularsComponent } from './components/circulars/circulars.component';
import { CircularsListComponent } from './components/circulars-list/circulars-list.component';
import { HttpClientModule } from '@angular/common/http';
import { AddCircularComponent } from './components/add-circular/add-circular.component';
import { CircularDetailComponent } from './components/circular-detail/circular-detail.component';
import { LayoutModule } from '../layout/layout.module';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { CircularsRoutingModule } from './circulars-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PdfViewerComponent } from './components/pdf-viewer/pdf-viewer.component';
import { NgxHijriGregorianDatepickerModule } from 'ngx-hijri-gregorian-datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
const routes: Routes = [
  {
    path: '',
    component: CircularsComponent,
  },
];

@NgModule({
  declarations: [AddCircularComponent,CircularDetailComponent,CircularsListComponent,CircularsComponent, PdfViewerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    FormsModule,
    LayoutModule,HttpClientModule,MatDatepickerModule,CircularsRoutingModule,TranslateModule,PdfViewerModule 
    ,NgxHijriGregorianDatepickerModule,NgxPaginationModule,MatSlideToggleModule,MatProgressSpinnerModule
  ],
  exports : [CircularsListComponent,AddCircularComponent],
  providers: [],
})
export class CircularsModule {}
