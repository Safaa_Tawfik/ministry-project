import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { config, Observable, ReplaySubject } from 'rxjs';
import { ToasterService } from 'src/app/services/toasterService.service';
import { CircularService } from '../../services/circular.service';
import { DateFormatterService, DateType } from 'ngx-hijri-gregorian-datepicker';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {  TranslateService } from '@ngx-translate/core';
import { Utils } from '../../services/utils';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Component({
  selector: 'app-add-circular',
  templateUrl: './add-circular.component.html',
  styleUrls: ['./add-circular.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddCircularComponent implements OnInit {
  date: any;
  selectedDate: any;
  minhijri: NgbDateStruct = this.config.minhijri;
  maxhijri: NgbDateStruct = this.config.maxhijri;
  mingreg: NgbDateStruct =  this.config.mingreg;
  maxgreg : NgbDateStruct= this.config.maxgreg
  
  selectedDateType = DateType.Hijri; // or DateType.Gregorian
  circular = {
    number: '',
    title: '',
    circular_date: '',
    circular_hijri_date: '',
    description : '',
    file_content: {
      filename: '',
      filesize: '',
      mimetype: '',
      filebase64: '',
    },
  };
  submitted = false;

  file: any;

  base64Output: string = '';
  onFileSelected(event: any) {
    const file: File = event.target.files[0];

    if (file) {
      this.file = file;
      const formData = new FormData();
      formData.append('thumbnail', file);
      this.convertFile(event.target.files[0]).subscribe((base64) => {
        this.base64Output = base64;
      });
      //  const upload$ = this.http.post("/api/thumbnail-upload", formData);

      //upload$.subscribe();
    }
  }

  convertFile(file: File): Observable<string> {
    console.log('convertFile');
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event: any) =>
      result.next(btoa(event.target.result.toString()));
    return result;
  }
  constructor(
    private circularsvc: CircularService,
    private toaster: ToasterService,
    private translate: TranslateService,
    private datesvc: DateFormatterService,
    private config : ConfigurationService
  ) {}
  isloading: boolean = false;
  ngOnInit(): void {}

  showSuccessToaster() {
    console.log('Show success Toaster');
    this.toaster.show('success', 'Well done!', 'This is a success alert', 3000);
  }
  showErrorToaster() {
    console.log('Show Error Toaster');
    this.toaster.show('error', 'Check it out!', 'This is a error alert', 3000);
  }
  showWarningToaster() {
    this.toaster.show(
      'warning',
      'Check it out!',
      'This is a warning alert',
      3000
    );
  }

  savecircular(addcircularform: any): void {
    this.isloading = true;
    let greg :any;
    let hijri : any;
    if (this.selectedDate.year < 1800 ) //hijri 
     {
      hijri = Utils.GetFormattedDate(this.selectedDate);
      greg =Utils.GetFormattedDate(this.datesvc.ToGregorian(this.selectedDate));
    } else {
      greg = Utils.GetFormattedDate(this.selectedDate);
      hijri = Utils.GetFormattedDate(this.datesvc.ToHijri(this.selectedDate));
    }
    this.circular.circular_date = greg;
    this.circular.circular_hijri_date =hijri;
    this.circular.file_content = {
      filename: this.file.name,
      filesize:  Utils.formatBytes(this.file.size),
      mimetype: this.file.type,
      filebase64: this.base64Output,
    };
    this.circularsvc.create(this.circular).subscribe(
      (response) => {
        console.log(response);
        this.submitted = true;
        this.isloading = false;
        this.showSuccessToaster();
        addcircularform.reset();
        this.circularsvc.getUpdatedCircularList.next();
      },
      (error) => {
        this.isloading = false;
        this.showErrorToaster();
        console.log(error);
      }
    );
  }

  newcircular(): void {
    this.submitted = false;
    this.circular = {
      number: '',
      title: '',
      circular_date: '',
      circular_hijri_date: '',
      description : '',
      file_content: {
        filename: '',
        filesize: '',
        mimetype: '',
        filebase64: '',
      },
    };
  }
}
