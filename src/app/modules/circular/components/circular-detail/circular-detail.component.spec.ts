import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularDetailComponent } from './circular-detail.component';

describe('CircularDetailComponent', () => {
  let component: CircularDetailComponent;
  let fixture: ComponentFixture<CircularDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CircularDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
