import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularsListComponent } from './circulars-list.component';

describe('CircularsListComponent', () => {
  let component: CircularsListComponent;
  let fixture: ComponentFixture<CircularsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CircularsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
