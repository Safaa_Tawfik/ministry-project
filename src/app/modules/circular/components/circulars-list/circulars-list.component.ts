import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { CircularService } from '../../services/circular.service';
import {ThemePalette} from '@angular/material/core';
import { delay } from 'rxjs/operators';
@Component({
  selector: 'app-circulars-list',
  templateUrl: './circulars-list.component.html',
  styleUrls: ['./circulars-list.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class CircularsListComponent implements OnInit {
  isFetching = false;
  p = 1;
  count = 1;
  totalRecords = 0;
  circulars: any;
  currentcircular: any;
  currentIndex = -1;
  title = '';
  year = "0";
  @Input() isadmin = false;
  minhijri_year :number;
  mingreg_year :number;
  maxhijri_year :number;
  maxgreg_year :number;
  
  color: any = '#2FAB99';
  checked = false;
  disabled = false;
  constructor(
    private circularsvc: CircularService,
    private router : Router,
    public config : ConfigurationService
  ) {
    this.minhijri_year = config.minhijri_year;
  this.mingreg_year = config.mingreg_year;
  this.maxgreg_year = config.maxgreg_year;
  this.maxhijri_year = config.maxhijri_year;
  
  console.log(this.mingreg_year,this.maxgreg_year);
  }

  delete(circular: any) {
    this.circularsvc
      .delete({ id: circular.id, filename: circular.path })
      .subscribe(
        (res) => {
          console.log('deleted success');
          this.retrievecirculars();
        },
        (error) => {
          console.log('failed to delete');
        }
      );
  }
  ngOnInit(): void {
    this.retrievecirculars();
    this.circularsvc.getUpdatedCircularList.subscribe((res)=> {
      this.retrievecirculars();
    })
  }

  async retrievecirculars() {
    this.isFetching=true;
    let searchinfo =  {
      currentPageIndex: this.p - 1,
      pageSize : this.count,
    }
    this.circularsvc.SearchByQuery({...searchinfo , ...this.searchquery}).subscribe(
      (data) => {
        this.circulars = data.results;
        if(data.resultsinfo.totalsize)
        this.totalRecords = data.resultsinfo.totalsize;
        console.log(data);
        this.isFetching=false;
      },
      (error) => {
        console.log(error);
        this.isFetching=false;
      }
    );
  }

  refreshList(): void {
    this.retrievecirculars();
    this.currentcircular = null;
    this.currentIndex = -1;
  }

  setActivecircular(circular: any, index: any): void {
    this.currentcircular = circular;
    this.currentIndex = index;
  }

  removeAllcirculars(): void {
    this.circularsvc.deleteAll().subscribe(
      (response) => {
        console.log(response);
        this.retrievecirculars();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  searchquery : any ={};

  search(): void {
    this.isFetching=true;
    this.p=1;
    let query = { }
    if(this.title) query = {searchtext : this.title};
    if(this.year != '0') query = {...query , year:this.year};
    this.searchquery=query;
    console.log("search query =",query);
    console.log("search query =",this.year);
   let searchinfo =  {
    currentPageIndex: this.p - 1,
    pageSize : this.count,
  }
    this.circularsvc.SearchByQuery({...this.searchquery, ...searchinfo}).subscribe(
      (data) => {
        this.circulars =  data.results;
        this.totalRecords =data.resultsinfo.totalsize;
        console.log(data);
        this.isFetching=false;
      },
      (error) => {
        console.log(error);
        this.isFetching=false;
      }
    );
  }

//   downloadFile(circular: any): void {
// //this.router.navigate(['/viewer',circular.path])
// const url = this.router.serializeUrl(this.router.createUrlTree(['/viewer'], { queryParams: { filename : circular.path } }));
// console.log("urel = ",url)
// window.open(url, '_blank');
//     // this.circularsvc.getfile(url.path).subscribe((data) => { 
//     //   console.log(data);
//     // },(error)=>{
//     //   console.log(error);
//     // })
//   //  window.open(this.config.download + url.path, '_blank');
//   }
onChange(event :any) {
  this.p = event;
  this.retrievecirculars();
}
 autohide : boolean =true;

 counter(i: number) {
  return new Array(i);
}


counter2(i: number) {
  return new Array(i);
}
}
