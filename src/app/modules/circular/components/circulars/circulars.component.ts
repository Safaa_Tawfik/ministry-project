import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { CircularService } from '../../services/circular.service';

@Component({
  selector: 'app-circulars',
  templateUrl: './circulars.component.html',
  styleUrls: ['./circulars.component.scss']
})
export class CircularsComponent implements OnInit {
  constructor(private circularsvc: CircularService , private translate : TranslateService,public config : ConfigurationService) {}

  ngOnInit(): void {
   
  }
}

