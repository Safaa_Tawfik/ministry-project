import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { CircularService } from '../../services/circular.service';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.scss']
})
export class PdfViewerComponent implements OnInit {

  constructor( private config: ConfigurationService,private route: ActivatedRoute) { }
 pdfSrc = '';
  ngOnInit(): void {
    let filename= this.route.snapshot.paramMap.get('filename')
    this.pdfSrc= this.config.download + filename;
  }

}
