import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class CircularService {

  public getUpdatedCircularList = new ReplaySubject();
  constructor(private http: HttpClient,private config : ConfigurationService) { }

  getAll(): Observable<any> {
    return this.http.get(this.config.baseurl);
  }

  get(id : string): Observable<any> {
    return this.http.get(`${this.config.baseurl}/circulars/${id}`);
  }

  create(data : any): Observable<any> {
    return this.http.post(`${this.config.baseurl}/circulars`, data);
  }

  update(id : string, data :any): Observable<any> {
    return this.http.put(`${this.config.baseurl}/${id}`, data);
  }

  delete(circular: any): Observable<any> {
    return this.http.post(`${this.config.baseurl}/circulars/delete`,circular);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.config.baseurl);
  }


  SearchByQuery(query : any): Observable<any> {
    return this.http.post(`${this.config.baseurl}/search`,query);
  }
  getfile(title : string)  :Observable<any> {
    return this.http.get(`${this.config.download2}${title}`);

  }
}