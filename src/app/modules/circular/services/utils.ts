import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

export class Utils {
    static formatBytes(a :any, b = 2) {
    if (0 === a) return '0 Bytes';
    const c = 0 > b ? 0 : b,
      d = Math.floor(Math.log(a) / Math.log(1024));
    return (
      parseFloat((a / Math.pow(1024, d)).toFixed(c)) +
      ' ' +
      ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][d]
    );
  }

  static GetFormattedDate(date: NgbDateStruct) {
    console.log("GetFormattedDate date= ",date)
    let year = date.year;
    let month: any = date.month;
    let day: any = date.day;
    if (date.month < 10) month = '0' + date.month;
    if (date.day < 10) day = '0' + date.day;
    let result =year + '-' + month + '-' + day;
    console.log("result =",result);
    return result;
  }
}
