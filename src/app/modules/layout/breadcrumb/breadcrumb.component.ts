import { Component, Input, OnInit } from '@angular/core';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  @Input() showbreadcrumb =true;
  constructor() { }

  ngOnInit(): void {
  }

  isEnglish() :boolean {
    return ConfigurationService.isEng();
  }

  
  isArabic() :boolean {
    return ConfigurationService.isAr();
  }
}
