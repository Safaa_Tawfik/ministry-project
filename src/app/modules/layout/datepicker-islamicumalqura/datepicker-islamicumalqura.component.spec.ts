import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerIslamicumalquraComponent } from './datepicker-islamicumalqura.component';

describe('DatepickerIslamicumalquraComponent', () => {
  let component: DatepickerIslamicumalquraComponent;
  let fixture: ComponentFixture<DatepickerIslamicumalquraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatepickerIslamicumalquraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerIslamicumalquraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
