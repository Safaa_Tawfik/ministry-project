import { Component, OnInit, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ConfigurationService } from 'src/app/services/configuration.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  locale: string = 'en';
switchto : string = 'العربية';
  constructor(private translate: TranslateService,@Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {}

  switchLanguage(): void {
    this.locale = this.locale === 'ar' ? 'en':'ar';
    this.switchto  = this.locale === 'ar' ? 'English':'العربية';
    this.translate.use(this.locale);
    let htmlTag = this.document.getElementsByTagName("html")[0] as HTMLHtmlElement;
    htmlTag.dir = this.locale === "ar" ? "rtl" : "ltr";
    ConfigurationService.setLanguage(this.locale);
  }
}
