import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { BaseComponent } from './base/base.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgbdDatepickerIslamicumalqura } from './datepicker-islamicumalqura/datepicker-islamicumalqura.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [BaseComponent, HeaderComponent, FooterComponent, BreadcrumbComponent,NgbdDatepickerIslamicumalqura],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,TranslateModule ,NgbDatepickerModule
  ],
  exports : [BreadcrumbComponent,NgbdDatepickerIslamicumalqura],
  providers: []
})
export class LayoutModule { }
