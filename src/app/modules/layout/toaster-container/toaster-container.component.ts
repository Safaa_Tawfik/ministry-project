import { Component, OnInit } from '@angular/core';
import { Toast } from 'ngx-toastr';
import { ToasterService } from 'src/app/services/toasterService.service';

@Component({
  selector: 'app-toaster-container',
  templateUrl: './toaster-container.component.html',
  styleUrls: ['./toaster-container.component.scss'],
})
export class ToasterContainerComponent implements OnInit {

  toasts: any[] = [];
  toast$:any;
  constructor(private toaster: ToasterService) {
  this.toast$=  this.toaster.toast$;
  }
  
  ngOnInit() {
    this.toaster.toast$
      .subscribe(toast => {
        this.toasts = [toast, ...this.toasts];
        setTimeout(() => this.toasts.pop(), toast.delay || 6000);
      });
  }

  remove(index: number) {
    this.toasts = this.toasts.filter((v, i) => i !== index);
    //this.toasts.splice(index, 1);
  }

}
