import { Injectable } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { locale } from 'moment';
import { DateFormatterService } from 'ngx-hijri-gregorian-datepicker';
@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  constructor(private datesvc : DateFormatterService){
    this.minhijri_year  = 1400;
    this.minhijri = {year: this.minhijri_year , month : 1 , day:1};
    this.maxhijri =datesvc.GetTodayHijri();
    this.maxhijri_year =this.maxhijri.year;

    this.mingreg_year  = 1900;
    this.mingreg = {year: this.mingreg_year , month : 1 , day:1};
    this.maxgreg =datesvc.GetTodayGregorian();
    this.maxgreg_year =this.maxgreg.year;
   
  }
//  public baseurl  = 'http://localhost:8080/api/';
// public download  = 'http://localhost:8080/api/downloadFile/?title=';
//  public download2  = 'http://localhost:8080/api/download2/?fileName=';

 public baseurl  = 'https://ministry-project.herokuapp.com/api';
 public download  = 'https://ministry-project.herokuapp.com/api/downloadFile/?title=';
 public download2  = 'https://ministry-project.herokuapp.com/api/download2/?fileName=';

 static locale : string ='en';
  minhijri_year :number;
  minhijri: NgbDateStruct;
  maxhijri_year :number ;
  maxhijri: NgbDateStruct;

  
  mingreg_year :number;
  mingreg: NgbDateStruct;
  maxgreg_year :number ;
  maxgreg: NgbDateStruct;

  
  
 
  
 static setLanguage(lang : string): void {
this.locale =lang;

 }
 static isEng() : boolean {
   return this.locale === 'en' ;
 }

 static isAr() : boolean {
  return this.locale === 'ar' ;
}
}